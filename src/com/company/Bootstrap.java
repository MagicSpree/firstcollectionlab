package com.company;

import com.company.exercises.Comparison;
import com.company.exercises.Deletion;

import java.util.*;

public class Bootstrap {

    private Deletion deletion;
    private Comparison comparison;

    public void run() {
        initObjects();
        removeRepetition();
        compare();
    }

    private void initObjects() {
        deletion = new Deletion(List.of("adadad", '1', '1', 12, 30L, 12, "adadad"));
        comparison = new Comparison();
    }

    private void removeRepetition() {
        print("Массив до удаления повторяющихся элементов " + deletion.getListWithObject().toString());
        deletion.removeRepetitionsWithStream();
        print("Массив после удаления повторяющихся элементов " + deletion.getListWithObject().toString());
    }

    public void compare() {
        print("Время работы цикла с ArrayList равен: " + comparison.getTimeArrayList());
        print("Время работы цикла с LinkedList равен: " + comparison.getTimeLinkedList());
    }

    private void print(String message) {
        System.out.println(message);
    }
}
