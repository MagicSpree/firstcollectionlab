package com.company.exercises;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class Deletion {

    private Collection<Object> listWithObject;

    public Deletion(Collection<Object> listWithObject) {
        this.listWithObject = listWithObject;
    }

    public void removeRepetitionsWithStream() {
        listWithObject = deleteDuplicateWithStream(listWithObject);
    }

    public void removeRepetitionsWithSet() {
        listWithObject = deleteAllDuplicateWithSet(listWithObject);
    }

    private <T> Collection<T> deleteDuplicateWithStream(Collection<T> list) {
        return list.stream()
                   .distinct()
                   .collect(Collectors.toList());
    }

    private <T> Collection<T> deleteAllDuplicateWithSet(Collection<T> list) {
        return new LinkedHashSet<>(list);
    }

    public Collection<Object> getListWithObject() {
        return listWithObject;
    }

}
