package com.company.exercises;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Comparison {

    private ArrayList<Double> listString;
    private LinkedList<Double> linkedListDouble;
    private long startTime = 0;

    private final int COUNT_ELEMENTS = 1000000;
    private final int COUNT_ELEMENTS_FOR_EXAMPLE = 10000;

    public Comparison() {
        initAndFill();
    }

    private void initAndFill() {
        listString = new ArrayList<>();
        linkedListDouble = new LinkedList<>();

        for (int i = 0; i < COUNT_ELEMENTS; i++) {
            listString.add(Math.random());
            linkedListDouble.add(Math.random());
        }
    }

    public double getTimeArrayList() {
        startTime = System.currentTimeMillis();
        getElements(listString);
        return (System.currentTimeMillis() - startTime);
    }

    public double getTimeLinkedList() {
        startTime = System.currentTimeMillis();
        getElements(linkedListDouble);
        return (System.currentTimeMillis() - startTime);
    }

    private void getElements(List<Double> array) {
        for (int i = 0; i < COUNT_ELEMENTS_FOR_EXAMPLE; i++) {
            array.get((int) (Math.random() * (COUNT_ELEMENTS - 1)));
        }
    }
}
